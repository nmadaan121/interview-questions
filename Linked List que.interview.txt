 Linked List questions to be prepared for Interview purpose
	
1. Print the Middle of a given linked list
2. Flattening a linked list
3. Delete the elements in an linked list whose sum is equal to zero
4. Delete middle of linked list
5. Remove duplicate elements from sorted linked list
6. Add 1 to a number represented as a linked list
7. Reverse a linked list in groups of given size
8. Detect loop in linked list
9. Remove loop in linked list
10. Find nth node from the end of linked list
11. Function to check if a singly linked list is a palindrome
12. Reverse alternate k node in a singly linked list
13. Delete last occurrence of an item from linked list
14. Rotate a linked list.
15. Delete n nodes after m nodes of a linked list.
16. Merge a linked list into another linked list at alternate positions.
17. Write a function to delete a linked list.
18. Write a function to reverse the nodes of a linked list.
19. Why quicksort is preferred for arrays and merge sort for linked lists.
20. Explain the concept of linked list in java


String Questions for Interview Preparation



1. How do you reverse a given string in place? 
2. How do you print duplicate characters from a string? 
3. How do you check if two strings are anagrams of each other? 
4. How do you find all the permutations of a string? 
5. How can a given string be reversed using recursion? 
6. How do you check if a string contains only digits? 
7. How do you find duplicate characters in a given string? 
8. How do you count a number of vowels and consonants in a given string? 
9. How do you count the occurrence of a given character in a string? 
10. How do you print the first non-repeated character from a string? 
11. Given an array of strings, find the most frequent word in a given array, I mean, the string that appears the most in the array. In the case of a tie, the string that is the smallest (lexicographically) is printed. 
12. How do you reverse words in a given sentence without using any library method? 
13. How do you check if two strings are a rotation of each other?
14. How do you check if a given string is a palindrome? 
15. How do you find the length of the longest substring without repeating characters? 
16. Given string str, How do you find the longest palindromic substring in str? 
17. How to convert a byte array to String? 
18. how to remove the duplicate character from String?
19. How to find the maximum occurring character in given String? 
20. How do you remove a given character from String?