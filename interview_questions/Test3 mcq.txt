1.What is the output of the below code with arrays?
	static int[] nums; 
	public static void main(String args[])
	{
  	System.out.println(nums.length);
	}
A) 0
B) null
C) Compiler error
D) Runtime Exception

Ans: D

2.What will be the content of array variable table after executing the following code?
for(int i = 0; i < 3; i + +)
for(int j = 0; j < 3; j + +)
if(j = i) table[i][j] = 1;
else table[i][j] = 0;
      A)1 1 1
	0 1 1
	0 0 1
      B)1 0 0
	0 1 0
	0 0 1
      c)1 0 0
	1 1 0
	1 1 1
      D)0 0 1
        0 1 0
        1 0 0
Ans: B


3.We should not specify the array size 
if declaration and initialization are done at the same time.
A) FALSE
B) TRUE


4.What type of Exceptions can be ignored at compile time?

A) Runtime
B) Checked
C) Both
D) None
Ans : A

5.FileNotFoundException

A) extends IOException
B) is Compile time exception
C) Found in java.io package
D) All
Ans : D

6.In a circular queue, how do you increment the rear end of the queue?
	
A) rear++;	
B) (rear+1) % CAPACITY;
C) (rear % CAPACITY)+1;
D) rear–-;

Ans :B

7.Which one of the following is an application of Queue Data Structure?

A) When a resource is shared among multiple consumers.
B) When data is transferred asynchronously between two processes
C) Load Balancing
D) All of the above
Ans : D

8.Which of these exceptions are propagated automatically in java?

A)IOException
B)NullPointerException
C)ClassNotFoundException
D)SQLException

Ans : B

9.What does the following piece of code do?
 
	public int function()
	{	
		if(isEmpty())
		return -1;
	else
	    {
		int x;
		x = q[front];
		return x;
	    }
	}
A) Dequeue
B) Enqueue
C) Return the front element
D) None of the mentioned

Ans : C

10.What will be the output of following code ?
	System.out.print("Start ");
	try 
	{
    		System.out.print("Hello");
    		throw new FileNotFoundException();
	}
	System.out.print(" Catch Here ");
	catch(EOFException e) 
	{
    	System.out.print("End of file exception");
	}
	catch(FileNotFoundException e) 
	{
	System.out.print("File not found");
	}

A) The code will not compile.
B) Start Hello File Not Found.
C) Start Hello End of file exception.
D) Start Hello Catch Here File not found.
Answer: A 

11.If an index of an element is N, what is its actual position in the array?

A) N-1
B) N
C) N+1
D) N+2

Ans : C

12.What will be the output of following code ?

	import java.io.IOException;
	public class MyException{

	     public static void main(String []args){
        	try
		{
			throw new IOException();
        	}
        catch(IOException | Exception ae)
        {
            System.out.println("handled");
        }
    }
}

A) runtime exception
B) program will compile
C) program won't compile
D) handled

Ans : C

13.What exception thrown by parseInt() method?	
	
A)ArithmeticException	
B)ClassNotFoundException	
C)NullPointerException
D)NumberFormatException

Ans : D

14.What is the output of this program?
    class evaluate 
    {
        public static void main(String args[]) 
            {
	        int arr[] = new int[] {0 , 1, 2, 3, 4, 5, 6, 7, 8, 9};
	        int n = 6;
                n = arr[arr[n] / 2];
	        System.out.println(arr[n] / 2);
            } 
    }

A)3
B)0
C)6
D)1

Ans : 1

15.A linear list of elements in which deletion can be done from one end 
  and insertion can take place only at the other end is known as a ?

A)Queue
B)Stack
C)Tree
D)Linked list

Ans : A

16.The minimum number of stacks needed to implement a queue is

A)3
B)1
C)2
D)4

Ans : C

17.What is the output of below snippet?
   	Object[] arr = new String[3];
	arr[0] = new Integer(0);

A)ArrayIndexOutOfBoundsException	
B)ArrayStoreException	
C)Compilation Error	
D)Code runs successfully

Ans: B

18.Where is array stored in memory?

A)heap space
B)stack space
C)heap space and stack space
D)first generation memory

Ans: A

19.Which of the following best represents a queue data structure?

A) A pile of dirty dishes requiring washing
B) Requests on a single shared resource, like a printer
C) A line of people waiting to go to an amusement park ride
D) (B) and (C)

Ans : D


20.What is the output for the following code fragment?
       int arr[]= { 1, 3, 6, 9, 2, 5, 7};
       int que = arr[0];
       for (int k = 0; k < arr.length; k++)
       {
           if (arr[k] < que)
              que = arr[k];
       }
       System.out.print(que);

A) 2
B) 1
C) 1 3 6 9 2 5 7
D) 7

Ans :B