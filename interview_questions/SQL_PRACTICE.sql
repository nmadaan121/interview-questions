use prac;

		CREATE TABLE Manufacturers (
  Code INTEGER,
  Name VARCHAR(255) NOT NULL,
  PRIMARY KEY (Code)   
);

CREATE TABLE Products (
  Code INTEGER,
  Name VARCHAR(255) NOT NULL ,
  Price DECIMAL NOT NULL ,
  Manufacturer INTEGER NOT NULL,
  PRIMARY KEY (Code), 
  FOREIGN KEY (Manufacturer) REFERENCES Manufacturers(Code)
) ENGINE=INNODB;

INSERT INTO Manufacturers(Code,Name) VALUES(1,'Sony');
INSERT INTO Manufacturers(Code,Name) VALUES(2,'Creative Labs');
INSERT INTO Manufacturers(Code,Name) VALUES(3,'Hewlett-Packard');
INSERT INTO Manufacturers(Code,Name) VALUES(4,'Iomega');
INSERT INTO Manufacturers(Code,Name) VALUES(5,'Fujitsu');
INSERT INTO Manufacturers(Code,Name) VALUES(6,'Winchester');

INSERT INTO Products(Code,Name,Price,Manufacturer) VALUES(1,'Hard drive',240,5);
INSERT INTO Products(Code,Name,Price,Manufacturer) VALUES(2,'Memory',120,6);
INSERT INTO Products(Code,Name,Price,Manufacturer) VALUES(3,'ZIP drive',150,4);
INSERT INTO Products(Code,Name,Price,Manufacturer) VALUES(4,'Floppy disk',5,6);
INSERT INTO Products(Code,Name,Price,Manufacturer) VALUES(5,'Monitor',240,1);
INSERT INTO Products(Code,Name,Price,Manufacturer) VALUES(6,'DVD drive',180,2);
INSERT INTO Products(Code,Name,Price,Manufacturer) VALUES(7,'CD drive',90,2);
INSERT INTO Products(Code,Name,Price,Manufacturer) VALUES(8,'Printer',270,3);
INSERT INTO Products(Code,Name,Price,Manufacturer) VALUES (9,'Toner cartridge',66,3);
INSERT INTO Products(Code,Name,Price,Manufacturer) VALUES(10,'DVD burner',180,2);

select * from products;
select * from manufacturers;

select name from products;

select name, price from products;

select name,price from products where price<=200;
select * from products where price between 60 and 120;
select name,price*100 as price_in_cents from products;
select avg(price) from products;
select code,avg(price) from products where manufacturer=2;
select count(code) as count from products where price>=180;
select name,price from products where price>=180 order by price desc, name ;
select * from products inner join Manufacturers on products.Manufacturer=Manufacturers.code order by products.code;
select p.name,p.price,m.name from products p inner join  manufacturers m on p.manufacturer=m.code;
select manufacturer,avg(price)from products group by manufacturer;
select m.name,avg(p.price) from  products p inner join manufacturers m on p.manufacturer = m.code group by p.manufacturer;
select m.name,avg(p.price) from products p  join manufacturers m on p.manufacturer=m.code group by p.manufacturer having avg(p.price)>=150;

select name,min(price) from products;
select name, price from Products 
where price = (
select min(price)
from products);

select  Manufacturers.name ,products.name, price from products inner join manufacturers on products.Manufacturer=Manufacturers.Code group by Manufacturer having  max(price);


.17 Add a new product: Loudspeakers, $70, manufacturer 2.
-- 1.18 Update the name of product 8 to "Laser Printer".
-- 1.19 Apply a 10% discount to all products.
-- 1.20 Apply a 10% discount to all products with a price larger than or equal to $120.
